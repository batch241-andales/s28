// CRUD OPERATIONS
/*
- CRUD operations are the heart of any backend application
- CRUD stands for Create, Read, Update and Delete
- MongoDB deal with objects as it's structure for documents, we can easily create them by providing objects into our methods
*/

// CREATE: Inserting documents

// INSERT ONE
/*
Syntax:
	db.collectionName.insertOne({})

Inserting/Assigning values in JS Objects:
	object.object.method({object})
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
})

// INSERT MANY
/*
Syntax:
db.users.insertMany([
	{objectA},
	{objectB}
])
*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5878787",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "5552252",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}	

]);



// READ: FINDING DOCUMENTS

// FIND ALL
/*
Syntax:
	db.collectionName.find();
*/
db.users.find();

// Finding users with single argument
/*
Syntax:
	db.collectionName.find({field: value})
*/

db.users.find({firstName: "Stephen"});

// Finding users with multiple arguments
// With no matches
db.users.find({firstName: "Stephen", age: 20}); // 0 records - no match
// With matching one
db.users.find({firstName: "Stephen", age: 76});

// UPDATE: Finding Documents
// Repeat Jane to be updated
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
});

// UPDATE ONE
/*
Syntax:
	db.collectionName.updateOne({criteria}, {$set: (field: value)})
*/
db.users.updateOne(
	{firstName: "Jane"},{
		$set:{
			firstName: "Jane",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "00000",
				email: "janegates@gmail.com"
			},
			courses: ["AWS", "Google Cloud", "Azure"],
			department: "infrastracture",
			status: "active"
		} 

	}
	);

db.users.find({firstName: "Jane"});


// UPDATE MANY
db.users.updateMany(
		{department: "none"},
		{
			$set: {
				department: "HR"
			}
		}

);
db.users.find().pretty();

// REPLACE ONE
/*
- Can be used if replacing the whole document is necessar
- Syntax:
	d.collectionName.replaceOne({criteria}, {$set {field: value}})
*/

db.users.replaceOne(
	{lastName: "Gates"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Clinton"
		}
	}
);
db.users.find({firstName: "Bill"});

// DELETE: DELETING DOCUMENTS
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})

db.users.find({firstName: "Test"});

// DELETING a single document
/*
Syntax: 
	db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({firstName: "Test"})
db.users.find({firstName: "Test"});

db.users.deleteOne({age: 76})

// Delete many

db.users.deleteMany({
	courses: []
})
db.users.find();

// Delete all
// db.users.delete()
/*
- Be careful when using the "deleteMany" method. If no search criteria is provided, it will delete all documents
- DO NOT USE: db.collectionName.deleteMany()
- Syntax:
	db.collectionName.deleteMany({criteria})
*/

// ADVANCED QUERIES

// Query an embedded document
db.users.find({
	contact : {
        phone : "5875698",
        email : "janedoe@gmail.com"
    }
});


// Find the document with the email "janedoe@gmail.com"
// Querying on nested fields
db.users.find({"contact.email" : "janedoe@gmail.com"})

// Querying an array with exact element
db.users.find({courses : [ "Laravel", "React", "SASS"]})
//Quering an element without regard to order
db.users.find({courses : {$all: [ "Laravel", "React", "SASS"]}})

db.users.insert({
	namearr:[
			{
				namea: "Juan"
			},
			{
				nameb: "Tamad"
			}
		]
})

// find
db.users.find({
	namearr: {
		namea: "Juan"
	}
})